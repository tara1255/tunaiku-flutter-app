import 'package:flutter/material.dart';

class MyColor {
  static const green = Color(0xFF48A800); //rgb 72 168 0
  static const red = Color(0xFFDD1717); //rgb 221 23 23
  static const grey = Color(0xFF6D7278); //rgb 109 114 120
  static const grey2 = Color(0xFFCDD0D6); //rgb 205 208 214
  static const black = Color(0xFF000000); //rgb 0 0 0
  static const white = Color(0xFFFFFFFF); //rgb 255 255 255
  static const blackText1 = Color(0xFF25282B); //rgb 37 40 43
  static const blackText2 = Color(0xFF424242); //rgb 66 66 66
  static const greyAppBar = Color(0xFFF9F9F9); //rgb 249 249 249
  static const transparent = Colors.transparent;
}
