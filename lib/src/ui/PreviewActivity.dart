import 'package:flutter/material.dart';

class PreviewData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.lightBlue),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Preview Data',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.grey[100],
      ),
      body: SafeArea(
        child: Center(
          child: Text('Data Behasil Di Kirim'),
        ),
      )
    );
  }
}