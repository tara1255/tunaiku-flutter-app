import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ce_tunaiku/src/common/MyColor.dart';

class ModalVisitDate extends StatefulWidget {
  final DateTime value;
  final Function(DateTime) onSelected;

  ModalVisitDate({@required this.value, this.onSelected});

  @override
  _ModalVisitDateState createState() => _ModalVisitDateState();
}

class _ModalVisitDateState extends State<ModalVisitDate> {

  final _listVisitDate = List<DateTime>.generate(
      10,
      (i) => DateTime.utc(
            DateTime.now().year,
            DateTime.now().month,
            DateTime.now().day,
          ).add(Duration(days: i)));

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 24),
        decoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(16.0),
                topRight: const Radius.circular(16.0))),
        child: Wrap(
          children: <Widget>[
            Center(
              child: Container(
                height: 4,
                width: 40,
                decoration: BoxDecoration(
                    color: MyColor.grey2,
                    borderRadius:
                        BorderRadius.all(const Radius.circular(2.0))),
                margin:
                    EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 2),
              ),
            ),
            Center(
              child: Container(
                height: 4,
                width: 40,
                decoration: BoxDecoration(
                    color: MyColor.grey2,
                    borderRadius:
                        BorderRadius.all(const Radius.circular(2.0))),
              ),
            ),
            Container(
              child: Text(
                'Pilih Tanggal',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColor.blackText1,
                    fontSize: 20),
              ),
              padding: EdgeInsets.all(16),
            ),
            Container(
              color: MyColor.grey2,
              height: 1,
            ),
            Container(
              child: Text(
                'Pastikan tanggal yang dipilih bukan Hari Libur Nasional',
                style: TextStyle(fontSize: 14, color: MyColor.blackText2),
              ),
              padding: EdgeInsets.all(16),
            ),
            _horizontalListViewDate(),
          ],
        ));
  }

  Widget _horizontalListViewDate() {
    return SizedBox(
      height: 130,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 8, right: 8),
        itemCount: _listVisitDate.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, i) => _itemListViewDate(i),
      ),
    );
  }

  Widget _itemListViewDate(int i) => GestureDetector(
        onTap: () {
          Navigator.pop(context);
          widget.onSelected(_listVisitDate[i]);
        },
        child: Container(
          width: 70,
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: _listVisitDate[i] == widget.value ? MyColor.green : MyColor.grey,
              borderRadius: BorderRadius.all(const Radius.circular(8.0))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                DateFormat('MMM').format(_listVisitDate[i]),
                style: TextStyle(color: MyColor.white),
              ),
              Text(
                DateFormat('dd').format(_listVisitDate[i]),
                style: TextStyle(
                    color: MyColor.white, fontSize: 24, fontWeight: FontWeight.bold),
              ),
              Text(
                DateFormat('E').format(_listVisitDate[i]),
                style: TextStyle(color: MyColor.white),
              ),
              Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                  color: DateFormat('E').format(_listVisitDate[i]) == 'Sat' ||
                          DateFormat('E').format(_listVisitDate[i]) == 'Sun'
                      ? MyColor.red
                      : MyColor.green,
                  borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                ),
              ),
            ],
          ),
        ),
      );
}
