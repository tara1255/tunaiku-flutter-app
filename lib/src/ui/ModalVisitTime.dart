import 'package:flutter/material.dart';
import 'package:ce_tunaiku/src/common/MyColor.dart';

class ModalVisitTime extends StatefulWidget {
  final String value;
  final Function(String) onSelected;

  ModalVisitTime({@required this.value, this.onSelected});

  @override
  _ModalVisitTimeState createState() => _ModalVisitTimeState();
}

class _ModalVisitTimeState extends State<ModalVisitTime> {
  final List<String> _listVisitTime = [
    'Pagi 09.00 - 12.00',
    'Siang 12.00 - 15.00',
    'Sore 15.00 - 18.00'
  ];

  String _selectedVisitTime;

  @override
  void initState() {
    super.initState();
    setState(() {
      _selectedVisitTime = widget.value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 24),
        decoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(16.0),
                topRight: const Radius.circular(16.0))),
        child: Wrap(
          children: <Widget>[
            Center(
              child: Container(
                height: 4,
                width: 40,
                decoration: BoxDecoration(
                    color: MyColor.grey2,
                    borderRadius: BorderRadius.all(const Radius.circular(2.0))),
                margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 2),
              ),
            ),
            Center(
              child: Container(
                height: 4,
                width: 40,
                decoration: BoxDecoration(
                    color: MyColor.grey2,
                    borderRadius: BorderRadius.all(const Radius.circular(2.0))),
              ),
            ),
            Container(
              child: Text(
                'Pilih Waktu Kunjungan',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: MyColor.blackText1,
                    fontSize: 20),
              ),
              padding: EdgeInsets.all(16),
            ),
            Container(
              color: MyColor.grey2,
              height: 1,
            ),
            Container(
              child: Text(
                'Perkiraan waktu kunjungan tim Tunaiku',
                style: TextStyle(fontSize: 14, color: MyColor.blackText2),
              ),
              padding: EdgeInsets.all(16),
            ),
            _itemRadioButtonSelectTime(),
            Container(
              padding: EdgeInsets.all(16),
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  onPressed: _selectedVisitTime == ''
                      ? null
                      : () => {
                            setState(() {
                              Navigator.pop(context);
                              widget.onSelected(_selectedVisitTime);
                            })
                          },
                  color: MyColor.green,
                  child: Text(
                    'Pilih',
                    style: TextStyle(color: MyColor.white, fontSize: 16),
                  ),
                  padding: EdgeInsets.all(14),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _itemRadioButtonSelectTime() {
    return Column(
        children: _listVisitTime
            .map((it) => Row(
                  children: <Widget>[
                    Radio(
                        activeColor: MyColor.green,
                        value: it,
                        groupValue: _selectedVisitTime,
                        onChanged: (value) {
                          setState(() {
                            _selectedVisitTime = value;
                          });
                        }),
                    Text(
                      it,
                      style: TextStyle(color: MyColor.grey, fontSize: 16),
                    )
                  ],
                ))
            .toList());
  }
}
