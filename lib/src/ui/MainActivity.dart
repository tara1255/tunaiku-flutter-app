import 'package:ce_tunaiku/src/ui/PreviewActivity.dart';
import 'package:ce_tunaiku/src/ui/ModalVisitDate.dart';
import 'package:ce_tunaiku/src/ui/ModalVisitTime.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ce_tunaiku/src/common/MyColor.dart';
import 'package:ce_tunaiku/src/common/Utils.dart';

class MainActivity extends StatefulWidget {
  @override
  _MainActivityState createState() => _MainActivityState();
}

class _MainActivityState extends State {
  DateTime _selectedVisitDate;

  var _selectedDate = TextEditingController();
  var _selectedTime = TextEditingController();
  var _phoneNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var body = SafeArea(
        child: ListView(
      padding: EdgeInsets.all(16),
      children: <Widget>[
        Text(
          'Buat Jadwal Bertemu Tim Tunaiku',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
            color: MyColor.black,
          ),
        ),
        SizedBox(height: 8),
        Text(
          'Tim Tunaiku akan mengunjungi Anda untuk proses tanda tangan kontrak pinjaman.',
        ),
        SizedBox(
          height: 24,
        ),
        Center(
          child: Image.asset(
            'assets/images/IcForm.png',
            height: 148,
            width: 220,
          ),
        ),
        SizedBox(height: 24),
        Text(
          'Tanggal Kunjungan',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            height: 1.5,
          ),
        ),
        Container(
          child: TextFormField(
            focusNode: AlwaysDisabledFocusNode(),
            onTap: () => _modalVisitDate(),
            controller: _selectedDate,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Pilih Tanggal Kunjungan',
                suffixIcon: Image.asset('assets/images/IcKontak.png')),
          ),
        ),
        SizedBox(height: 36),
        Text(
          'Waktu Kunjungan',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            height: 1.5,
          ),
        ),
        Container(
          child: TextFormField(
            focusNode: AlwaysDisabledFocusNode(),
            onTap: () => _modalVisitTime(),
            controller: _selectedTime,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Pilih Waktu Kunjungan',
                suffixIcon: Image.asset('assets/images/IcClock.png')),
          ),
        ),
        SizedBox(height: 36),
        Text(
          'Nomor Handphone kedua (opsional)',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            height: 1.5,
          ),
        ),
        Container(
          child: TextFormField(
            controller: _phoneNumber,
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Cth: 0823xxxxxxxx'),
          ),
        ),
        SizedBox(
          height: 44,
        ),
        Container(
          child: SizedBox(
            width: double.infinity,
            child: RaisedButton(
              onPressed: _selectedDate.text == '' || _selectedTime.text == ''
                  ? null
                  : () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PreviewData()))
                      },
              color: MyColor.green,
              child: Text(
                'Selanjutnya',
                style: TextStyle(color: MyColor.white),
              ),
              padding: EdgeInsets.all(14),
            ),
          ),
        ),
      ],
    ));

    var appBar = AppBar(
      centerTitle: true,
      title: Text(
        'Form Kunjungan',
        style: TextStyle(color: MyColor.blackText1),
      ),
      backgroundColor: MyColor.greyAppBar,
    );

    return Scaffold(
      backgroundColor: MyColor.white,
      appBar: appBar,
      body: body,
    );
  }

  _modalVisitDate() {
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColor.transparent,
        builder: (builder) {
          return Container(
            child: ModalVisitDate(
              value: _selectedVisitDate,
              onSelected: (DateTime val) => {
                setState(() {
                  _selectedVisitDate = val;
                  _selectedDate.text = DateFormat('dd MMMM yyyy').format(val);
                })
              },
            ),
          );
        });
  }

  _modalVisitTime() {
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColor.transparent,
        builder: (builder) {
          return ModalVisitTime(
            value: _selectedTime.text,
            onSelected: (String val) {
              setState(() {
                _selectedTime.text = val;
              });
            },
          );
        });
  }
}
